import { Component, OnInit } from "@angular/core";
import { ServerService } from "../server.service";
import { ActivatedRoute, ParamMap, Router } from "@angular/router";
import { AuthService } from "../auth.service";

@Component({
  selector: "app-team",
  templateUrl: "./team.component.html",
  styleUrls: ["./team.component.css"]
})
export class TeamComponent implements OnInit {
  id: Number;
  name: String;
  country: String;
  coachName: String;
  foundationDate: String;
  logo: String;
  players: [];

  constructor(
    private server: ServerService,
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.id = +this.route.snapshot.paramMap.get("id");
  }

  ngOnInit() {
    this.server.request("GET", `/teams/${this.id}`).subscribe((team: any) => {
      this.name = team.name;
      this.country = team.country;
      this.coachName = team.coachName;
      this.foundationDate = team.foundationDate;
      this.logo = team.logo;
      this.players = team.players;
    });
  }

  deleteClick() {
    this.server
      .request("DELETE", `/teams/${this.id}`)
      .subscribe((team: any) => {
        this.router.navigateByUrl("/teams");
      });
  }
}
