import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { ServerService } from '../server.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-team-form',
  templateUrl: './team-form.component.html',
  styleUrls: ['./team-form.component.css']
})
export class TeamFormComponent implements OnInit {
  teamForm: FormGroup;

  constructor(private server: ServerService, private fb: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.teamForm = this.fb.group({
      name: ['', [Validators.required]],
      country: [''],
      coachName: [''],
      foundationDate: ['', [Validators.required]],
      players: this.fb.array([this.initPlayer()])
    })

    this.teamForm.controls
  }

  initPlayer() {
    return this.fb.group({
      name: ['', [Validators.required]],
      nationality: [''],
      birthDate: ['', [Validators.required]],
    });
}

  addPlayer() {
    const control = <FormArray>this.teamForm.get('players');
    control.push(this.initPlayer());
  }

  removePlayer(index: number) {
    const control = <FormArray>this.teamForm.get('players');
    control.removeAt(index);
  }

  async onSubmit() {
    let data = JSON.stringify(this.teamForm.getRawValue());

    this.server.request('POST', `/teams`, data).subscribe((team: any) => {
      this.router.navigateByUrl(`/teams/${team.id}`);
    })
  }
}
