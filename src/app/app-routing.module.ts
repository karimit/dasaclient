import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TeamsComponent } from './teams/teams.component';
import { TeamComponent } from './team/team.component';
import { NewTeamComponent } from './new-team/new-team.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  { path: '', component: TeamsComponent },
  { path: 'teams', component: TeamsComponent },
  { path: 'teams/new', component: NewTeamComponent },
  { path: 'teams/:id', component: TeamComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
