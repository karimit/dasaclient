import { Component, OnInit } from '@angular/core';
import { ServerService } from '../server.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.css']
})
export class TeamsComponent implements OnInit {
  teams: Object;

  constructor(private server: ServerService, private authService: AuthService) { }

  ngOnInit() {
    this.server.request('GET', '/teams').subscribe((teams: any) => {
      this.teams = teams;
    });
  }

}
