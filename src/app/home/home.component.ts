import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  appTitle: string = 'DasaClient';
  public isAuthenticated = false;

  constructor() { }

  ngOnInit() {
  }

}
